defmodule TrixtaSpaces.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TrixtaSpaces.Worker.start_link(arg)
      # {TrixtaSpaces.Worker, arg},
      # TrixtaSpaces.SpacesSupervisor
    ]

    # TrixtaStorage.open_table(:spaces_table)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrixtaSpaces.Supervisor]
    ret = Supervisor.start_link(children, opts)

    # load any existing spaces
    # TrixtaStorage.traverse(:spaces_table, fn {_space_id, space} ->
    #   case TrixtaSpaces.start_space(space) do
    #     {:error, {:already_started, _pid}} -> :continue
    #     {:error, {:unknown_error}, _} -> :continue
    #     {:ok, _pid, _metric} -> :continue
    #   end
    # end)

    ret
  end
end
